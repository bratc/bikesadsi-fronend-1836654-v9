import { HttpParams } from '@angular/common/http';

export const createRequestParams = (req?: any): HttpParams => {
    let options: HttpParams = new HttpParams();
    if(req){
        Object.keys(req).forEach(key => {
            options = options.set(key, req[key] )
        });
    }
    return options;
}

/// array
/*
let datos = [1,2,3,5]; // for

let dataClient = { //
    name: 'Carlos',
    age: 23
}

dataClient['name'] // Carlos
*/
