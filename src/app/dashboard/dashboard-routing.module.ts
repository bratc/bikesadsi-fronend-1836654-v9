import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
 {
   path: '',
   component: MainComponent,
  children: [
    {
      path: '',
      component: HomeComponent
    },
    {
      path: 'bikes',
      loadChildren: () => import('../components/bikes/bikes.module')
      .then(modulo => modulo.BikesModule)
    },
    {
      path: 'sales',
      loadChildren: () => import('../components/sales/sales.module')
      .then(m => m.SalesModule)
    }
  ]
 }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
