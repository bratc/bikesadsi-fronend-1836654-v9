
export interface IDataSendBike {
    model?: string;
    serial?: string;
    price?: number;
    status?: boolean;
    documentNumber?: string;
    idClient?: number;
}
