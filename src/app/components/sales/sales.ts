import { IBike } from '../bikes/interfaces/bike';

export interface ISale {
    id?: number,
    date?: Date,
    clientId?: number,
    clientName?: string,
    bikeId?: number,
    bikeSerial?: string,
    client?: any,
    bike?: IBike
}
