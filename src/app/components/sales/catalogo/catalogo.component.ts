import { Component, OnInit } from '@angular/core';
import { SalesService } from '../sales.service';
import { BikesService } from '../../bikes/bikes.service';
import { IBike } from '../../bikes/interfaces/bike';
import { ISale } from '../sales';
import { ClientsService } from '../../clients/clients.service';
import { IClient } from '../../clients/clients';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.styl']
})
export class CatalogoComponent implements OnInit {
  bikesList: IBike[];
  salesListCar: ISale[] = [];
  clientsList: IClient[] = [];
  clientSelectedTemp: IClient;


  pageSize = 8;
  pageNumber = 0;

  listPages = []; // list pages for buttons
  searchForm = this.fb.group({
    document: ''
  });

  constructor(
    private salesService: SalesService,
    private bikesService: BikesService,
    private clientService: ClientsService,
    private fb: FormBuilder
    ) { 
    }

  ngOnInit(): void {
    this.initPagination(this.pageNumber);
  
  }


  initPagination(page: number): void {
    this.bikesService.queryBikes({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      console.warn('DATOS ',res);
      this.bikesList = res.content;
      this.formatPage(res.totalPages);
    });
  }


  selectedClient(client: any): void {
    console.warn('Selected Client',client);
    this.clientSelectedTemp = client;
  }

  addToCar(bike: IBike): void {
    console.warn('Selected ',bike);
    this.salesListCar.push({
      bike: bike,
      client: this.clientSelectedTemp
    });


  }
  confirmSale() {
    console.warn('Enviar datos al backend',this.salesListCar);
    console.warn('JSHON STRING ',JSON.stringify(this.salesListCar));
    this.salesService.createMultiple(this.salesListCar)
    .subscribe((res: any) => {
      this.salesListCar = [];
      console.log('Save ok');
    })
  }

  searchClient(event: any): void {
    console.warn('Event ',event);
    this.clientService.findClientByDocumentNumber({
      'document.contains': event.query
    })
    .subscribe(res => {
      this.clientsList = res;
    });
  }
  

  private formatPage(countPages: number): void {
    this.listPages = [];
    for(let i = 0; i < countPages; i++) {
      this.listPages.push(i);
    }
  }
}
