import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ISale } from './sales';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(private http: HttpClient) { }

  public query(): Observable<ISale[]> {
    return this.http.get<ISale[]>(`${environment.END_POINT}/api/sales`)
    .pipe(map(res => {
      return res;
    }));
  }

  public createMultiple(listSales: ISale[]): Observable<ISale> {
    return this.http.post<ISale>(`${environment.END_POINT}/api/sales/multiple`,listSales)
    .pipe(map(res => {
      return res;
    }));
  }
}
