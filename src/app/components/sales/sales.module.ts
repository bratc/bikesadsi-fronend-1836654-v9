import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesRoutingModule } from './sales-routing.module';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SalesListComponent } from './sales-list/sales-list.component';
import { MainSalesComponent } from './main-sales/main-sales.component';
import {AutoCompleteModule} from 'primeng/autocomplete';


@NgModule({
  declarations: [CatalogoComponent, SalesListComponent, MainSalesComponent],
  imports: [
    CommonModule,
    SalesRoutingModule,
    ReactiveFormsModule,
    AutoCompleteModule
  ]
})
export class SalesModule { }
