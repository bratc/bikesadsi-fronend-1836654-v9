import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { SalesListComponent } from './sales-list/sales-list.component';
import { MainSalesComponent } from './main-sales/main-sales.component';

/**
 * Routing
 */
const routes: Routes = [
  {
    path: '',
    component: MainSalesComponent,
    children: [
      {
        path: '',
        component: CatalogoComponent
      },
      {
        path: 'sales-list',
        component: SalesListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
