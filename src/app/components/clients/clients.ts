export interface IClient {
    id?: number,
    name?: string,
    document?: string,
    email?: string,
    phoneNumber?: string,
    documentType?: string,
    image?: string,
    imageContentType?: string
}
