import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IClient } from './clients';
import { createRequestParams } from 'src/app/utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IClient[]> {
    let params = createRequestParams(req);
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/clients`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }
  
  public findClientByDocumentNumber(req?: any): Observable<IClient[]> {
    let params = createRequestParams(req);
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/clients/find-by-document`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }

/*
  public getBikeBySerial(document: string): Observable<IClient>{
    let params = new HttpParams();
     return this.http.get<IClient>(`${environment.END_POINT}/api/bikes/find-bike-by-serial`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
    */
}
