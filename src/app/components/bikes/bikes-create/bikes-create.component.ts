import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { IBike } from '../interfaces/bike';
import { IDataSendBike } from 'src/app/shared/models/data-send-bike';

@Component({
  selector: 'app-bikes-create',
  templateUrl: './bikes-create.component.html',
  styleUrls: ['./bikes-create.component.styl']
})
export class BikesCreateComponent implements OnInit {
  bike: IBike;

  statusSearchSerial: boolean = false;

  results: any;

  formSearchBike = this.formBuilder.group({
      serial: [''],
      document: ''
  });

  bikeFormGroup = this.formBuilder.group({
    model: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(4)
    ])],
    price: [''],
    serial: [''],
    status: true,
  });


  dataSendBikesCreate: IDataSendBike;

  constructor(private formBuilder: FormBuilder, private bikesService: BikesService) {}

  ngOnInit() {
  }

  saveBike() {
    console.log('DATOS', this.bikeFormGroup.value);
    this.dataSendBikesCreate = {
      documentNumber: '11223344',
      idClient: 2,
      model: this.bikeFormGroup.value.model,
      price: this.bikeFormGroup.value.price,
      serial: this.bikeFormGroup.value.serial,
      status: true
    };


    this.bikesService.saveBike(this.dataSendBikesCreate)
    .subscribe(res => {
        console.log('SAVE OK ', res);
    }, error => {
      console.error('Error', error);
    });
  }

  searchBike() {
    console.warn('Data', this.formSearchBike.value);
    this.bikesService.getBikeBySerial(this.formSearchBike.value.serial)
    .subscribe(res => {
      console.warn('response bike by Serial ',res);
      this.bike = res;
      this.statusSearchSerial = true;
    }, error => {
      console.warn('No encontrado ', error);
      this.bike = null;
      this.statusSearchSerial = false;
    });
  }

  changeInputSwitch(event: any): void {
    console.log('EVENT LIST', event);
  }

  
  changeSelect(): void {
    console.log(typeof this.bikeFormGroup.value.status);
    let status = true;
    if(this.bikeFormGroup.value.status == '1'){
      status = true;
    } else {
      status = false;
    }
    console.log("valor Status ",status);
  }
  search(event: any): void {
    console.warn('search ',event);
  }
}
