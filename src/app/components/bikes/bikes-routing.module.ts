import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BikesListComponent } from './bikes-list/bikes-list.component';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';
import { BikesViewComponent } from './bikes-view/bikes-view.component';

// localhost:4200/dashboard/bikes/bikes-list

const routes: Routes = [
  {
    path: '',
    component: BikesListComponent

  },
  {
    path: 'bikes-create',
    component: BikesCreateComponent
  },
  {
    path: 'bikes-update/:id',
    component: BikesUpdateComponent
  },
  {
    path: 'bikes-view',
    component: BikesViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikesRoutingModule { }
