import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IBike } from '../interfaces/bike';

@Component({
  selector: 'app-bikes-update',
  templateUrl: './bikes-update.component.html',
  styleUrls: ['./bikes-update.component.styl']
})
export class BikesUpdateComponent implements OnInit {
  BikeFormGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private bikesService: BikesService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) {
    this.BikeFormGroup = this.formBuilder.group({
      id: [''],
      model: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(4)
      ])],
      price: [''],
      serial: ['']
    });
  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log('ID PATH', id);
    this.bikesService.getBikeById(id)
    .subscribe(res => {
      console.log('get data ok', res);
      this.loadForm(res);
    });
  }

  saveBike() {
    console.log('DATOS', this.BikeFormGroup.value);
    this.bikesService.updateBike(this.BikeFormGroup.value)
    .subscribe(res => {
        console.log('UPDATE OK ', res);
        this.router.navigate(['/bikes/bikes-list']);
    }, error => {
      console.error('Error', error);
    });
  }

  private loadForm(Bike: IBike){
    this.BikeFormGroup.patchValue({
      id: Bike.id,
      model: Bike.model,
      price: Bike.price,
      serial: Bike.serial
    });
  }
}
