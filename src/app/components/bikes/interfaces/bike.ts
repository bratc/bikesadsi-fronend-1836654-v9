export interface IBike {
    id: number;
    model: string;
    price: string;
    serial: string;
}
