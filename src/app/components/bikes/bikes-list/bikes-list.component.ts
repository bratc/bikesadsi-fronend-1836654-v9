import { Component, OnInit } from '@angular/core';
import { IBike } from '../interfaces/bike';
import { BikesService } from '../bikes.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-bikes-list',
  templateUrl: './bikes-list.component.html',
  styleUrls: ['./bikes-list.component.styl']
})
export class BikesListComponent implements OnInit {

  public bikesList: IBike[];

  pageSize = 8;
  pageNumber = 0;

  listPages = []; // list pages for buttons

  filterForm = this.fb.group({
    model: new FormControl({value: 'ddd', disabled: true}),
    serial: '',
    createDate: ''
  });

  filters = {};
  constructor(private bikesService: BikesService, private fb: FormBuilder) {
   }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  } // end onInit

  initPagination(page: number): void {
    this.pageNumber = page;
    this.filters['pageNumber'] = this.pageNumber;
    this.filters['pageSize'] = this.pageSize;

    this.bikesService.queryBikes(this.filters)
    .subscribe((res: any) => {
      console.warn('DATOS ',res);
      this.bikesList = res.content;
      this.formatPage(res.totalPages);
    });
  }
  loadFilterEvent(): void {
    console.warn('View values Form ',this.filterForm.value);

    this.formatFilters();
    this.initPagination(0);
  }

  private formatFilters(): void {
    if (this.filterForm.value.model) {
      this.filters['model'] = this.filterForm.value.model;
    } else {
      delete this.filters['model'];
    }

    if (this.filterForm.value.serial) {
      this.filters['serial'] = this.filterForm.value.serial;
    } else {
      delete this.filters['serial'];
    }
  }
  deleteItem(id: string) {
    console.warn('ID ',id);
    this.bikesService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }


  private formatPage(countPages: number): void {
    this.listPages = [];
    for(let i = 0; i < countPages; i++) {
      this.listPages.push(i);
    }
  }

}
