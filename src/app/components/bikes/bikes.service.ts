import { Injectable } from '@angular/core';
import { IBike } from './interfaces/bike';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { createRequestParams } from 'src/app/utils/request.utils';
import { IDataSendBike } from 'src/app/shared/models/data-send-bike';
@Injectable({
  providedIn: 'root'
})
export class BikesService {

  constructor(private http: HttpClient) { }

  public queryBikes(req?: any): Observable<IBike[]> {
    let params = createRequestParams(req);
    return this.http.get<IBike[]>(`${environment.END_POINT}/api/bikes`,{params: params})
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * this...
   * @param IBike 
   * 
   */
  public saveBike(dataSendBike: IDataSendBike): Observable<IBike> {
    return this.http.post<IBike>(`${environment.END_POINT}/api/bikes/bike-with-client`, dataSendBike)
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * This method is for getting one IBike for id
   * @param id 
   */

  public getBikeById(id: string): Observable<IBike> {
    return this.http.get<IBike>(`${environment.END_POINT}/api/bikes/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }


  /**
   * this... UPDATE
   * @param IBike 
   * 
   */
  public updateBike(IBike: IBike): Observable<IBike> {
    return this.http.put<IBike>(`${environment.END_POINT}/api/bikes`, IBike)
      .pipe(map(res => {
        return res;
      }));
  }

  public getBikeBySerial(serial: string): Observable<IBike>{
    let params = new HttpParams();
    params = params.append('serial',serial);
    console.warn('PARAMS ',params);
    return this.http.get<IBike>(`${environment.END_POINT}/api/bikes/find-bike-by-serial`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }

  public deleteItem(id: string): Observable<IBike> {
    return this.http.delete<IBike>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }



} // close key class Service